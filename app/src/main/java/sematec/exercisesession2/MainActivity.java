package sematec.exercisesession2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    Intent goTo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void Btn_signup(View V) {
       goTo = new Intent(MainActivity.this, SignUpActivity.class);
        startActivity(goTo);
    }

    public void Btn_signin(View V) {
        goTo = new Intent(MainActivity.this, ComingSoon.class);
        startActivity(goTo);
    }
}
